package com.jemurai.training.validators;

public interface DateValidator {
	
	public boolean test(String input);
	
}