package com.jemurai.training.xsstest

import spock.lang.Specification

import com.jemurai.training.xsstest.XSSValidator
import com.jemurai.training.xsstest.XSSValidatorImpl

class XSSValidatorTest extends Specification {

	def "Null null safe Id Compare"() {
		given:
		XSSValidator validator = new XSSValidatorImpl();

		expect:
		false == validator.test("anything");
		true == validator.test("0");
	}

}

