package com.jemurai.training.validators

import spock.lang.Specification

import com.jemurai.training.validators.ZIPValidator
import com.jemurai.training.validators.ZIPValidatorImpl

class ZIPValidatorTest extends Specification {

	def "Verify ZIPValidator rejects appropriately and passes appropriately"() {
		given:
		ZIPValidator validator = new ZIPValidatorImpl();

		expect:
		false == validator.test(null);
		false == validator.test("");
		false == validator.test("A");
		false == validator.test("0");
		false == validator.test("0000");
		false == validator.test("000000");
		false == validator.test("00000-000");
		false == validator.test("00000-00000");
		false == validator.test("0000-00000");

		false == validator.test("0A000");
		false == validator.test("0A000-0000");
		false == validator.test("0?000");
		false == validator.test("0?000-0000");
		false == validator.test("0&000");
		false == validator.test("0&000-0000");
		false == validator.test("0\"000");
		false == validator.test("0\"000-0000");

		true == validator.test("00000");
		true == validator.test("00000-0000");

		// NEED to be able to do this ... 
		// true == validator.test("01223-1232");

		// true == validator.test("60615");
		// true == validator.test("60615-2013");


	}

}

