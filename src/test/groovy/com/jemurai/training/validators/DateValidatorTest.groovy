package com.jemurai.training.validators

import spock.lang.Specification

import com.jemurai.training.validators.DateValidator
import com.jemurai.training.validators.DateValidatorImpl

class DateValidatorTest extends Specification {

	def "Good pass"() {
		given:
		DateValidator validator = new DateValidatorImpl();

		expect:
		false == validator.test(null);
		false == validator.test("");
		false == validator.test("A");
		false == validator.test("0");
		false == validator.test("01124124123123");
		false == validator.test("May 8, 2015\"; cat /etc/passwd");
		false == validator.test("May 8, 2015 <img src=hi onerror=alert(yo)/>");

		// What would more nasty cases look like?


		true == validator.test("May 8, 2015");
		true == validator.test("Feb-21-2015");

		// Add more real dates.  Maybe ok to choose a format ... ?



	}

}

