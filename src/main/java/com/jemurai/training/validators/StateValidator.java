package com.jemurai.training.validators;

public interface StateValidator {
	
	public boolean test(String input);
	
}