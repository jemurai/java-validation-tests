# README #

This codebase is intended for building with Gradle and really just testing.

### What is this repository for? ###

* The purpose of this repo is to hold some broken tests for very basic java validators.
* Version 1.0

### How do I get set up? ###

>gradle test

### Who do I talk to? ###

* Matt Konda (@mkonda) / mkonda - at - jemurai.com