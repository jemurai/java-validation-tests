package com.jemurai.training.xsstest;

public interface XSSValidator {
	
	public boolean test(String input);
	
}