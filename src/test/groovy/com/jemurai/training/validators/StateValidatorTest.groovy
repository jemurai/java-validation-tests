package com.jemurai.training.validators

import spock.lang.Specification

import com.jemurai.training.validators.StateValidator
import com.jemurai.training.validators.StateValidatorImpl

class StateValidatorTest extends Specification {

	def "Good pass"() {
		given:
		StateValidator validator = new StateValidatorImpl();

		expect:
		false == validator.test(null);
		false == validator.test("");
		false == validator.test("A");
		false == validator.test("0");

		false == validator.test("Ma");
		false == validator.test("Mass");
		false == validator.test("Massachusetts");
		false == validator.test("Ct");
		false == validator.test("Ct.");
		false == validator.test("Conn");
		false == validator.test("Connecticut");

		false == validator.test("M;");
		false == validator.test("M-");

		true == validator.test("IL");
		true == validator.test("CT");

		// Other states?


	}

}

