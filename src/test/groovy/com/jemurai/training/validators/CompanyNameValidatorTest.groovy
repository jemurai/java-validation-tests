package com.jemurai.training.validators

import spock.lang.Specification

import com.jemurai.training.validators.CompanyNameValidator
import com.jemurai.training.validators.CompanyNameValidatorImpl

class CompanyNameValidatorTest extends Specification {

	def "Good pass"() {
		given:
		CompanyNameValidator validator = new CompanyNameValidatorImpl();

		expect:
		false == validator.test(null);
		false == validator.test("");
		false == validator.test("A");
		false == validator.test("0");

		// What are some good nasty things ... 

		true == validator.test("Real Company");
		true == validator.test("Jemurai, LLC");
		true == validator.test("owasp.org");

	}

}
