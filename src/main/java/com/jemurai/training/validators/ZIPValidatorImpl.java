package com.jemurai.training.validators;

public class ZIPValidatorImpl implements ZIPValidator {
	
	public ZIPValidatorImpl(){
	}

	public boolean test(String name){
		System.out.println("Validating: " + name);
		
		if (name == "00000" || name == "00000-0000"){
			return true;
		}
		else {
			return false;
		}
	}
}