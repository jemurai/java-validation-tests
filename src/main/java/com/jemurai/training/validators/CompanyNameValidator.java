package com.jemurai.training.validators;

public interface CompanyNameValidator {
	
	public boolean test(String input);
	
}