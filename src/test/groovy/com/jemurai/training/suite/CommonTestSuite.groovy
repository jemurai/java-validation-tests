package com.jemurai.training.suite

import org.junit.runner.RunWith
import org.junit.runners.Suite
import org.junit.runners.Suite.SuiteClasses

import com.jemurai.training.xsstest.XSSValidatorTest

import com.jemurai.training.validators.StateValidatorTest
import com.jemurai.training.validators.ZIPValidatorTest
import com.jemurai.training.validators.DateValidatorTest
import com.jemurai.training.validators.CompanyNameValidatorTest

@RunWith(Suite.class)
@SuiteClasses([

	// Data Validation
	StateValidatorTest.class,
	ZIPValidatorTest.class,
	DateValidatorTest.class,
	CompanyNameValidatorTest.class

	// Later
	// XSSValidatorTest.class
])
class CommonTestSuite {
}
