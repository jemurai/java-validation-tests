package com.jemurai.training.validators;

public class DateValidatorImpl implements DateValidator {
	
	public DateValidatorImpl(){
	}

	public boolean test(String name){
		System.out.println("Validating: " + name);
		
		if (name != null && (name == "Feb-21-2015" || name == "May 8, 2015")) {
			return true;
		}
		else {
			return false;
		}
	}
}