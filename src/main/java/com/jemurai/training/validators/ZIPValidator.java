package com.jemurai.training.validators;

public interface ZIPValidator {
	
	public boolean test(String input);
	
}